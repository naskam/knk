<form  id="update_form" method="POST" action="{{ url('/ajax/member_group/update/'.$id) }}">
  {!! csrf_field() !!}
  {{ method_field('PATCH') }}
  <div class="form-group">
    <label>سەردێر :</label>
    <input type="text" class="form-control" name="type"  value="{{ $item->type }}">
  </div>
  <div class="form-group">
    <label>سەردێری مێنۆ :</label>
    <input type="text" class="form-control" name="menu_title" value="{{ $item->menu_title }}">
  </div>
  <div class="form-group">
    <label>ناوەڕۆک :</label>
    <textarea name="body" id="summernote" cols="30" rows="10" class="form-control">{{ $item->body }}</textarea>
  </div>
  <button type="submit" class="btn btn-success">پاشکەوت کردنی گۆڕانکاری</button>
</form>
<script type="text/javascript">
$(document).ready(function(){
   $('#update_form #summernote').summernote({
      height: 200
   }); 
 })
</script>