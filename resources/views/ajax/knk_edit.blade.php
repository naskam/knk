<form  id="update_form" method="POST" action="{{ url('/ajax/knk/update/'.$id) }}">
  {!! csrf_field() !!}
  {{ method_field('PATCH') }}
  <div class="form-group">
    <label>سەردێر :</label>
    <input type="text" class="form-control" name="title" value="{{ $item->title }}">
  </div>
  <div class="form-group">
    <label>سەردێری مێنۆ :</label>
    <input type="text" class="form-control" name="menu_title" value="{{ $item->menu_title }}">
  </div>
  <div class="form-group">
    <label>ناو و پاشناو :</label>
    <input type="text" class="form-control" name="tags" value="{{ $item->tags }}">
  </div>
 <div class="form-group">
    <label>ناوەرۆک :</label>
    <textarea class="form-control" name="body" id="summernote">{{ $item->body }}</textarea>
  </div>
  <button type="submit" class="btn btn-success">پاشکەوت کردنی گۆڕانکاری</button>
</form>
<script type="text/javascript">
$(document).ready(function(){
   $('#update_form #summernote').summernote({
      height: 200
   }); 
 });
</script>