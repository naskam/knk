<form  id="update_form" method="POST" action="{{ url('/ajax/cases/update/'.$id) }}">
  {!! csrf_field() !!}
  {{ method_field('PATCH') }}
  <div class="form-group">
    <label>سەردێر :</label>
    <input type="text" class="form-control" name="title" value="{{ $item->title }}">
  </div>
  <div class="form-group">
    <label>تاگەکان :</label>
    <input type="text" class="form-control" name="tags" value="{{ $item->tags }}">
  </div>
  <div class="form-group">
    <label>ناوەرۆک :</label>
    <textarea name="body" id="summernote" cols="30" rows="10">{{ $item->body }}</textarea>
  </div>
  <button type="submit" class="btn btn-success">پاشکەوت کردنی گۆڕانکاری</button>
</form>
<script type="text/javascript">
$(document).ready(function(){
   $('#update_form #summernote').summernote({
      height: 200
   }); 
 });
</script>