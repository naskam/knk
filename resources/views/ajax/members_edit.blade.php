<form  id="update_form" method="POST" action="{{ url('/ajax/member/update/'.$id) }}">
  {!! csrf_field() !!}
  {{ method_field('PATCH') }}
  <div class="form-group">
    <label>جۆری ئەندامێتی :</label>
    <select name="group_id" class="form-control">
      @foreach($group as $group_item)
        <option value="{{ $group_item->id }}" 
        <?php if ($group_item->id == $item->group_id) {
          echo 'selected';
        } ?>
        >
        {{ $group_item->type }}</option>
        @endforeach
    </select>
  </div>
  <div class="form-group">
    <label>سەردێر :</label>
    <input type="text" class="form-control" name="title" value="{{ $item->title }}">
  </div>
  <div class="form-group">
    <label>ناو و پاشناو :</label>
    <input type="text" class="form-control" name="name" value="{{ $item->name }}">
  </div>
 <div class="form-group">
    <label>ئیمەیل :</label>
    <input type="email" class="form-control" name="email" value="{{ $item->email }}">
  </div>
 <div class="form-group">
    <label>لئنک بۆ دەرەوە :</label>
    <input type="text" class="form-control" name="link" value="{{ $item->link }}">
  </div>
  <button type="submit" class="btn btn-success">پاشکەوت کردنی گۆڕانکاری</button>
</form>