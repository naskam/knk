<form  id="update_form" method="POST" autocomplete="off" action="{{ url('/ajax/user/update/'.$id) }}">
  {!! csrf_field() !!}
  {{ method_field('PATCH') }}
  <div class="form-group">
    <label>بەکارهێنەر :</label>
    <input type="text" class="form-control" name="name" value="{{ $item->name }}">
  </div>
  <div class="form-group">
    <label>ئیمەیل :</label>
    <input type="email" class="form-control" autocomplete="off" name="email" value="{{ $item->email }}">
  </div>
  <div class="form-group">
    <label>ووشەی نەهێنی :</label>
    <input type="password" class="form-control" autocomplete="off" name="password" value="">
  </div>
  <button type="submit" class="btn btn-success">پاشکەوت کردنی گۆڕانکاری</button>
</form>
