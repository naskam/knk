  <div class="form-group">
    <label>ناو :</label>
    <p>{{ $item->name }}</p>
  </div>
  <div class="form-group">
    <label>ئیمەیل :</label>
    <p>{{ $item->email }}</p>
  </div>
  <div class="form-group">
    <label>پەیام :</label>
    <p>{{ strip_tags($item->message) }}</p>
  </div>