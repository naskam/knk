<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap-rtl.css" crossorigin="anonymous">
    <!-- include summernote css/js-->
    <link href="/css/summernote.css" rel="stylesheet">
    <style>
        body {
            font-family: 'tahoma';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    ک ن ک
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/dashboard') }}">دەستپێک</a></li>
                    <li >
                        <a href="{{ URL::to('dashboard/kurdistan') }}">کوردستان</a>
                    </li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> ئەندامان <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/dashboard/member_group') }}">دەستەی ئەندامان</a></li>
                                <li><a href="{{ url('/dashboard/members') }}">ئەندامان</a></li>
                            </ul>
                    </li>
                    <li><a href="{{ url('dashboard/knk')}}">ک ن ک</a></li>
                    <li><a href="{{ url('dashboard/declarations') }}">ڕاگەیاندن</a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> تایبەت <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/dashboard/cases') }}">دۆسیە</a></li>
                                <li><a href="{{ url('/dashboard/analyze') }}">شیکردنەوە</a></li>
                            </ul>
                    </li>
                    <li><a href="{{ url('/dashboard/messages') }}">پەیامەکان</a></li>
                    <li><a href="{{ url('/dashboard/users') }}">بەکارهێنەران</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">چوونە ژورەوە</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>چوونە دەرەوە</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="container">
    <div class="row">
        @yield('content')
    </div>
</div>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/summernote.min.js"></script>
@yield('scripts')
</body>
</html>
