@extends('layouts.app')
@section('title')
Administration :: ک ن ک
@stop
@section('content')
	<div class="col-md-12">
    <div id="notification" class="alert alert-info">
      {{ Session('message') }}
    </div>
		<div class="panel panel-default">
                <div class="panel-heading">بە شی بەکارهێنەران</div>
                <div class="panel-body">
                <div class="col-md-12">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/users') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">سەردێر</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">سەردێری مێنۆ</label>

                            <div class="col-md-8">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">تاگەکان</label>

                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    زیاد بکە
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                    </div>
                    </div>
          <div class="col-md-12">
						<table class="table" id="table">
						<thead>
							<tr>
								<th>ناو</th>
								<th>ئیمەیل</th>
								<th>گۆڕانکاری</th>
							</tr>
							@foreach($users as $item)
							<tr>
								<td>{{ $item->name }}</td>
								<td>{{ $item->email }}</td>
								<td>
									<a href="{{ $item->id }}" id="edit" class="btn btn-xs btn-primary">گۆڕانکاری</a>
								</td>
							</tr>
							@endforeach
						</thead>
						</table>
                    <?php echo $users->render(); ?></div>
	</div>

        <!-- Item Edit  Modal -->
        <div class="modal fade" id="edit_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">داخستن</span></button>
                <h4 class="modal-title" id="myModalLabel">گۆڕانکاری بەشی بەهارهێنەر</h4>
              </div>
              <div class="modal-body" id="edit_form">

            </div>
          </div>
        </div>
        </div>
@stop

<!-- JavaScript Section -->

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    /* SummerNote for Textarea */
    $('#summernote').summernote({
        height:300
    });
    var base_url='{!! url('/') !!}';
    $('#notification').hide();
    /* Edit Form */
    $('#table').on('click','#edit',function(e){
            var item = $(this).attr('href');
             $('edit_form').html('');
            $.ajax({
              url:base_url + '/ajax/user/edit/' + item,
              type:'get',
              success:function(data){
                  $('#edit_form').html(data);
              },
              error:function(data){
                  $('#edit_form').html(data);
              }
              });
            $('#edit_item_modal').modal("show");
            e.preventDefault();
        });
    /* Update form */
        $('#edit_form').on('submit','#update_form',function(e){
            $.ajax({
              url:$(this).attr('action'),
              data:$(this).serialize(),
              type:'POST',
              success:function(data){
                  $('#edit_item_modal').modal("hide");
                  $('#notification').html(data).show().fadeOut(5000);
              },
              error:function(data){
                  $('#edit_item_modal').modal("hide");
                  $('#notification').html(data).show().fadeOut(5000);
              }
              });
            e.preventDefault();
        });
        /* Delete Button */
        $('#table').on('click','#delete',function(e){
            id = $(this).attr("href");
            var object = $(this);
            if (confirm('ئایا دڵنیای لە ڕەش کردنەوە ؟')) {
              $.ajax({
                url:base_url + '/ajax/knk/delete/'+id,
                type:'POST',
                data:{ _method:'delete' , _token:'{{ csrf_token() }}'},
                success:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                  var td=object.parent();
                  var tr=td.parent();
                  tr.fadeOut(1000).remove();
                },
                error:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                }
              });
        }
          e.preventDefault();
       });
    });
</script>
@stop
