@extends('layouts.app')
@section('title')
Administration :: پەیامەکان
@stop
@section('content')
<div id="notification" class="alert alert-info">
      {{ Session('message') }}
    </div>
	<table class="table" id="table">
	  <thead>
		<tr>
			<th>نێو</th>
			<th>ئیمەیل</th>
			<th>#</th>
		</tr>
	  </thead>
		@foreach($contact as $item)
			<tr>
				<td>{{ $item->name }}</td>
				<td>{{ $item->email }}</td>
				<td>
					<a href="{{ $item->id }}" id="show" class="btn btn-xs btn-primary">بینینی پەیام</a>
					<a href="{{ $item->id }}" id="delete" class="btn btn-xs btn-danger"> ڕەش کردنەوە</a>
				</td>
			</tr>
		@endforeach
	</table>
	{{ $contact->render() }}

	    <!-- Item Edit  Modal -->
        <div class="modal fade" id="edit_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">داخستن</span></button>
                <h4 class="modal-title" id="myModalLabel">پەیام</h4>
              </div>
              <div class="modal-body" id="edit_form">
                   
            </div>
          </div>
        </div>
        </div>
@stop
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    var base_url='{!! url('/') !!}';
    $('#notification').hide();
    /* Show Form */
    $('#table').on('click','#show',function(e){
            var item = $(this).attr('href');
             $('edit_form').html('');
            $.ajax({
              url:base_url + '/ajax/message/show/' + item,
              type:'get',
              success:function(data){
                  $('#edit_form').html(data);
              },
              error:function(data){
                  $('#edit_form').html(data);
              }
              });
            $('#edit_item_modal').modal("show");
            e.preventDefault();
        });
        /* Delete Button */
        $('#table').on('click','#delete',function(e){
            id = $(this).attr("href");
            var object = $(this);
            if (confirm('ئایا دڵنیای لە ڕەش کردنەوە ؟')) {
              $.ajax({
                url:base_url + '/ajax/message/delete/'+id,
                type:'POST',
                data:{ _method:'delete' , _token:'{{ csrf_token() }}'},
                success:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                  var td=object.parent();
                  var tr=td.parent();
                  tr.fadeOut(1000).remove();
                },
                error:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                }
              });
        }
          e.preventDefault();
       });
    });
</script>
@stop