@extends('layouts.app')
@section('title')
Administration :: ئەندامان
@stop
@section('content')
	<div class="col-md-12">
     <div id="notification" class="alert alert-info">
      {{ Session('message') }}
    </div>
		<div class="panel panel-default">
                <div class="panel-heading">زیاد کردنی آیتم بەشی ئەندامان</div>
                <div class="panel-body">
                <div class="col-md-10">
                @if(Session::has('message'))
                    <div class="alert alert-warning" role="alert">
                        {{ Session::pull('message') }}
                    </div>
                @endif
				<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/dashboard/members') }}" >
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">جۆۆری ئەندامێتی</label>

                            <div class="col-md-8">
                                <select name="group_id" class="form-control">
                                    @foreach($member_group as $item)
                                        <option value="{{ $item->id }}">{{ $item->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">سەردێر</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">ناو و پاشناو</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">ئیمەیل</label>

                            <div class="col-md-8">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">وێنە</label>

                            <div class="col-md-8">
                                <input type="file" class="form-control" name="image" value="{{ old('image') }}">

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">لینک بۆ دەرەوە</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="link" value="{{ old('link') }}">

                                @if ($errors->has('link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    زیاد بکە 
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-12">
						<table class="table" id="table">
						<thead>
							<tr>
								<th>سەردێر</th>
								<th>سەردێری مێنو</th>
                <th>ئیمەیل</th>
								<th>وێنە</th>
								<th>گۆڕانکاری</th>
							</tr>
							@foreach($members as $item)
							<tr>
								<td>{{ $item->title }}</td>
								<td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
								<td><img src="/uploads/members/thumb/{{ $item->image }}" alt="" style="max-width:50px;"></td>
								<td>
									<a href="{{ $item->id }}" id="edit" class="btn btn-xs btn-primary">گۆڕانکاری</a>
									<a href="{{ $item->id }}" id="delete" class="btn btn-xs btn-danger">ڕەش کردنەوە</a>
								</td>
							</tr>
							@endforeach
						</thead>
						</table>
                    <?php echo $members->render(); ?></div>
	</div>

        <!-- Item Edit  Modal -->
        <div class="modal fade" id="edit_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">داخستن</span></button>
                <h4 class="modal-title" id="myModalLabel">گۆڕانکاری بەشی ئەندام</h4>
              </div>
              <div class="modal-body" id="edit_form">
                   
            </div>
          </div>
        </div>
        </div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    var base_url='{!! url('/') !!}';
    $('#notification').hide();
    /* Edit Form */
    $('#table').on('click','#edit',function(e){
            var item = $(this).attr('href');
             $('edit_form').html('');
            $.ajax({
              url:base_url + '/ajax/member/edit/' + item,
              type:'get',
              success:function(data){
                  $('#edit_form').html(data);
              },
              error:function(data){
                  $('#edit_form').html(data);
              }
              });
            $('#edit_item_modal').modal("show");
            e.preventDefault();
        });
    /* Update form */
        $('#edit_form').on('submit','#update_form',function(e){
            $.ajax({
              url:$(this).attr('action'),
              data:$(this).serialize(),
              type:'POST',
              success:function(data){
                  $('#edit_item_modal').modal("hide");  
                  $('#notification').html(data).show().fadeOut(5000);
              },
              error:function(data){
                  $('#edit_item_modal').modal("hide");
                  $('#notification').html(data).show().fadeOut(5000);
              }
              });
            e.preventDefault();
        });
        /* Delete Button */
        $('#table').on('click','#delete',function(e){
            id = $(this).attr("href");
            var object = $(this);
            if (confirm('ئایا دڵنیای لە ڕەش کردنەوە ؟')) {
              $.ajax({
                url:base_url + '/ajax/member/delete/'+id,
                type:'POST',
                data:{ _method:'delete' , _token:'{{ csrf_token() }}'},
                success:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                  var td=object.parent();
                  var tr=td.parent();
                  tr.fadeOut(1000).remove();
                },
                error:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                }
              });
        }
          e.preventDefault();
       });
    });
</script>
@stop