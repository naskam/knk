@extends('layouts.app')
@section('title')
Administration :: دەستەی ئەندامان
@stop
@section('content')
	<div class="col-md-12">
    <div id="notification" class="alert alert-info">
      {{ Session('message') }}
    </div>
		<div class="panel panel-default">
                <div class="panel-heading">زیاد کردنی آیتم بەشی گرۆپی ئەندامان</div>
                <div class="panel-body">
                <div class="col-md-10">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/member_group') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">جۆری گرۆپی ئەندامان</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="type" value="{{ old('type') }}">
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('menu_title') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">سەردێری مێنۆ</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="menu_title" value="{{ old('menu_title') }}">

                                @if ($errors->has('menu_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('menu_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">ناوەرۆک</label>

                            <div class="col-md-8">
								<textarea name="body" id="summernote" class="form-control" cols="30" rows="10"></textarea>
                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    زیاد بکە 
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <br>
						<table class="table" id="table">
						<thead>
							<tr>
								<th>سەردێر</th>
								<th>سەردێری مێنو</th>
								<th>ئیمەیل</th>
								<th>گۆڕانکاری</th>
							</tr>
							@foreach($member_group as $item)
							<tr>
								<td>{{ $item->type }}</td>
								<td>{{ $item->menu_title }}</td>
								<td>{{ substr(strip_tags($item->body),0,100) }}</td>
								<td>
									<a href="{{ $item->id }}" id="edit" class="btn btn-xs btn-primary">گۆڕانکاری</a>
									<a href="{{ $item->id }}" id="delete" class="btn btn-xs btn-danger">ڕەش کردنەوە</a>
								</td>
							</tr>
							@endforeach
						</thead>
						</table>
                    <?php echo $member_group->render(); ?></div>
	</div>

        <!-- Item Edit  Modal -->
        <div class="modal fade" id="edit_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">داخستن</span></button>
                <h4 class="modal-title" id="myModalLabel">گۆڕانکاری بەشی کوردستان</h4>
              </div>
              <div class="modal-body" id="edit_form">
                   
            </div>
          </div>
        </div>
        </div>
@stop
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
   $('body #summernote').summernote({
      height: 300
   });
    var base_url='{!! url('/') !!}';
    $('#notification').hide();
    /* Edit Form */
    $('#table').on('click','#edit',function(e){
            var item = $(this).attr('href');
             $('edit_form').html('');
            $.ajax({
              url:base_url + '/ajax/member_group/edit/' + item,
              type:'get',
              success:function(data){
                  $('#edit_form').html(data);
              },
              error:function(data){
                  $('#edit_form').html(data);
              }
              });
            $('#edit_item_modal').modal("show");
            e.preventDefault();
        });
    /* Update form */
        $('#edit_form').on('submit','#update_form',function(e){
            $.ajax({
              url:$(this).attr('action'),
              data:$(this).serialize(),
              type:'POST',
              success:function(data){
                  $('#edit_item_modal').modal("hide");  
                  $('#notification').html(data).show().fadeOut(5000);
              },
              error:function(data){
                  $('#edit_item_modal').modal("hide");
                  $('#notification').html(data).show().fadeOut(5000);
              }
              });
            e.preventDefault();
        });
        /* Delete Button */
        $('#table').on('click','#delete',function(e){
            id = $(this).attr("href");
            var object = $(this);
            if (confirm('ئایا دڵنیای لە ڕەش کردنەوە ؟')) {
              $.ajax({
                url:base_url + '/ajax/member_group/delete/'+id,
                type:'POST',
                data:{ _method:'delete' , _token:'{{ csrf_token() }}'},
                success:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                  var td=object.parent();
                  var tr=td.parent();
                  tr.fadeOut(1000).remove();
                },
                error:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                }
              });
        }
          e.preventDefault();
       });
    });
</script>
@stop