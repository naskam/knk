@extends('layouts.app')
@section('title')
Administration :: ڕاگەیاندن
@stop
@section('content')
	<div class="col-md-12">
    <div id="notification" class="alert alert-info">
      {{ Session('message') }}
    </div>
		<div class="panel panel-default">
                <div class="panel-heading"> بەشی ڕاگەیاندن</div>
                <div class="panel-body">
                <div class="col-md-10">
				<form class="form-horizontal" role="form" method="POST"  enctype="multipart/form-data" action="{{ url('/dashboard/declarations') }}">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">سەردێر</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">ناوەرۆک</label>

                            <div class="col-md-10">
                                <textarea name="body" id="summernote" class="form-control" cols="30" rows="10">{{ old('body') }}</textarea>

                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">تاگەکان</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="tags" value="{{ old('tags') }}">

                                @if ($errors->has('tags'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">وێنە</label>

                            <div class="col-md-8">
                                <input type="file" class="form-control" name="image" value="{{ old('image') }}">

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    زیاد بکە 
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-8">
						<table class="table" id="table">
						<thead>
							<tr>
								<th>سەردێر</th>
								<th>سەردێری مێنو</th>
								<th>تاگ</th>
								<th>گۆڕانکاری</th>
							</tr>
							@foreach($declarations as $item)
							<tr>
								<td>{{ $item->title }}</td>
								<td><?php echo substr(strip_tags($item->body),0,100).'...';  ?></td>
                                <td>{{ $item->email }}</td>
								<td><img src="/uploads/declarations/thumb/{{ $item->image }}" alt="" style="max-width:50px;"></td>
								<td class="col-md-3">
									<a href="{{ $item->id }}" id="edit" class="btn btn-xs btn-primary">گۆڕانکاری</a>
									<a href="{{ $item->id }}" id="delete" class="btn btn-xs btn-danger">ڕەش کردنەوە</a>
								</td>
							</tr>
							@endforeach
						</thead>
						</table>
            {{ $declarations->render() }}
             </div>
	</div>

        <!-- Item Edit  Modal -->
        <div class="modal fade" id="edit_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">داخستن</span></button>
                <h4 class="modal-title" id="myModalLabel">گۆڕانکاری بەشی ڕاگەیاندن</h4>
              </div>
              <div class="modal-body" id="edit_form">
                   
            </div>
          </div>
        </div>
        </div>
@stop

<!-- Javascript -->

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    /* SummerNote   */
    $('#summernote').summernote({
        height:300
    });
    var base_url='{!! url('/') !!}';
    $('#notification').hide();
    /* Edit Form */
    $('#table').on('click','#edit',function(e){
            var item = $(this).attr('href');
             $('edit_form').html('');
            $.ajax({
              url:base_url + '/ajax/declaration/edit/' + item,
              type:'get',
              success:function(data){
                  $('#edit_form').html(data);
              },
              error:function(data){
                  $('#edit_form').html(data);
              }
              });
            $('#edit_item_modal').modal("show");
            e.preventDefault();
        });
    /* Update form */
        $('#edit_form').on('submit','#update_form',function(e){
            $.ajax({
              url:$(this).attr('action'),
              data:$(this).serialize(),
              type:'POST',
              success:function(data){
                  $('#edit_item_modal').modal("hide");  
                  $('#notification').html(data).show().fadeOut(5000);
              },
              error:function(data){
                  $('#edit_item_modal').modal("hide");
                  $('#notification').html(data).show().fadeOut(5000);
              }
              });
            e.preventDefault();
        });
        /* Delete Button */
        $('#table').on('click','#delete',function(e){
            id = $(this).attr("href");
            var object = $(this);
            if (confirm('ئایا دڵنیای لە ڕەش کردنەوە ؟')) {
              $.ajax({
                url:base_url + '/ajax/declaration/delete/'+id,
                type:'POST',
                data:{ _method:'delete' , _token:'{{ csrf_token() }}'},
                success:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                  var td=object.parent();
                  var tr=td.parent();
                  tr.fadeOut(1000).remove();
                },
                error:function(data){
                  $('#notification').html(data).show().fadeOut(5000);
                }
              });
        }
          e.preventDefault();
       });
    });
</script>
@stop