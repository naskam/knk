@extends('web.layout')
@section('title')
ک ن ک : ئەندامان
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="ئەندامانی کۆنگرەی نەتەوەیی کوردستان">
<meta name="keywords" content="ئەندامانی , کۆنگرەی , نەتەوەیی , کوردستان">
@stop
@section('content')
<div class="row"> 
<h3 class="text-center">.: ئەندامان :.</h3><hr style="">
  @foreach($members as $member)
  <div class="col-md-2">
    <div class="team-player">
	    <img src="/uploads/members/{{ $member->image }}" alt="Thumbnail Image" class="img-rounded img-responsive">
	    <p class="title text-center" style="margin-top:5px;">{{ $member->name }}
			<br><small>{{ $member->title }}</small>
	    </p>
		@if($member->link)
			<p class="text-center" ><a href="http://{{ $member->link }}" target="_blank" class="btn btn-info btn-sm btn-raised">لینک بۆ دەڕەوە</a></p>
		@endif
	</div>
   </div>
  @endforeach
</div>
@stop
