@extends('web.layout')
@section('title')
ک ن ک : پەیوەندی
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="کۆنگری نەتەوەیی کوردستان پەیوەندی">
<meta name="keywords" content="کۆنگرە , نەتەوەیی , پەیوەندی">
@stop
@section('content')
<div class="section landing-section">
  @if(Session('message')) 
    <div class='alert alert-info'>
    {{ Session('message') }}
    </div>
  @endif
@if (session('msg'))
    <div class="alert alert-danger">
        {{ session('msg') }}
    </div>
@endif

      <div class="row">
          <div class="col-md-8 ">
              <h2 class="text-center title">پەیوەندی </h2>
  <h4 class="text-center description">دەتوانن لە رێگەی فورمی خوارەوە پەیامی خۆتان بگەیێننە ئێمە</h4>
          <form class="contact-form" method="POST" action="{{ url('contact') }}">
          {!! csrf_field() !!}
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label">نێوی ئێوە</label>
                      <input type="text" name="name" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label">ئیمەیلی ئێوە</label>
                      <input type="email" name="email" class="form-control">
                    </div>
                  </div>
              </div>
            <div class="form-group label-floating">
              <label class="control-label pull-left">پەیامەکەتان</label>
              <textarea class="form-control" rows="4" name="message"></textarea>
            </div>
            <div class="row">
              <div class="col-md-6">
                {!! app('captcha')->display(); !!}
              </div>
              <div class="col-md-4 text-center">
                  <button class="btn btn-success btn-raised btn-round">
                    پەیامەکەت بنێرە
                  </button>
              </div>
            </div>
          </form>
          </div>
          <div class="col-md-4">
            <br>
            <blockquote>
            <address>
              <strong>کۆنگرەی نەتەوەیی کوردستان</strong><hr>
              ئەوروپا - برۆکسێل<br>
              تلفن : +322555545<br>
              فاکس : 0032-26476849
            </address>
              <hr>
            <address>
              <p>ک ن ک ڕۆژهەڵاتی ناوەڕاست</p><br>
              <a href="mailto:knkbasur@gmail.com">knkbasur@gmail.com</a>
              <hr>
               <strong>ک ن ک ڕۆژئاوا</strong><br>
              <a href="mailto:knkrojava@gmail.com">knkrojava@gmail.com</a>
            </address>
            </blockquote>
          </div>
      </div>

  </div>

@stop
