@extends('web.layout')
@section('title')
ک ن ک : {{ $kurdistan->title }}
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $kurdistan->title }}">
<meta name="keywords" content="{{ $kurdistan->tags }}">
@stop
@section('content')
  <div class="row">
	<div class="blog-item">
	<div class="blog-content">
	<h3 class="text-info">{{$kurdistan->title}}</h3></a>
	<div class="entry-meta">
	<span class="small"></span> -
	<span class="small"></span>
	</div><br>
	<div><?php echo $kurdistan->body; ?></div>
	</div>
	</div>
	<br>
  </div>
@stop
