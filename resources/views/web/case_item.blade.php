@extends('web.layout')
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $case->title }}">
<meta name="keywords" content="{{ $case->tags }}">
@stop
@section('title')
ک ن ک : {{ $case->title }}
@stop
@section('content')
	<div class="col-md-9">
	<br>
	<div class="panel panel-default">
	<div class="panel-heading" style="background-image:url(/uploads/cases/{{ $case->image }});height:300px;background-repeat:no-repeat;background-size:100%;"></div>
	    <div class="panel-body">
	    	<h3>{{ $case->title }}</h3>
	    	<?php echo $case->body; ?>
	    </div>
    </div>
	</div>	
@stop