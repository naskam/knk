@extends('web.layout')
@section('title')
ک ن ک : {{ $knk->title }}
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $knk->title }}">
<meta name="keywords" content="{{ $knk->tags }}">
@stop
@section('content')
<div class="row">
	<h3>{{ $knk->title }}</h3>
    <?php echo $knk->body ?>
</div>
@stop
