@extends('web.layout')
@section('title')
ک ن ک : {{ $declaration->title }}
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $declaration->title }}">
<meta name="keywords" content="{{ $declaration->tags }}">
@stop
@section('content')
	<div class="col-md-9">
	<br>
	<div class="panel panel-default">
	<div class="panel-heading" style="background-image:url(/uploads/declarations/{{ $declaration->image }});height:300px;background-repeat:no-repeat;background-size:100%;"></div>
	    <div class="panel-body">
	    	<h3>{{ $declaration->title }}</h3>
	    	<?php echo $declaration->body; ?>
	    </div>
    </div>
	</div>	
@stop