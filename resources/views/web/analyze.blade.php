@extends('web.layout')
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="کۆنگرەی نەتەوەیی کوردستان : شرۆڤە">
<meta name="keywords" content="کوردستان , نەتەوە , کۆنگرە , شرۆڤە">
@stop
@section('title')
ک ن ک : شڕۆڤە
@stop
@section('content')
	<h3 class="text-center">.: شیکردنەوە :.</h3>
	<hr style="margin:5px 0px 12px 0px;border-style:dashed;border-color:#bbb;">
	@foreach($analyze as $item)
	<div class="col-md-3">
		<div class=" panel panel-default" >
			<div class="panel-heading" style="height:120px;background-repeat:no-repeat;background-size:100%;background-image:url('uploads/analyze/thumb/{{ $item->image }}');"></div>
			<div class="panel-body" style="min-height:80px !important">
				<a href="{{ URL::to('/analyze/'.$item->id) }}">{{ $item->title }}</a>
			</div>
		</div>
	</div>
	@endforeach
	<div class="clearfix"></div>
	<hr style="margin:12px 0px 5px 0px;border-style:dashed;border-color:#bbb;">
	<div class="col-md-12"><?php echo $analyze->render(); ?></div>
@stop