@extends('web.layout')
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $analyze->title }}">
<meta name="keywords" content="{{ $analyze->tags }}">
@stop
@section('title')
ک ن ک : {{ $analyze->title }}
@stop
@section('content')
	<div class="col-md-9">
	<br>
	<div class="panel panel-default">
	<div class="panel-heading" style="background-image:url(/uploads/analyze/{{ $analyze->image }});height:300px;background-repeat:no-repeat;background-size:100%;"></div>
	    <div class="panel-body">
	    	<h3>{{ $analyze->title }}</h3>
	    	<?php echo $analyze->body; ?>
	    </div>
    </div>
	</div>	
@stop