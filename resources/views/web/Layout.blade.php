<!doctype html>
<html lang="ku">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	@yield('meta')
	<title>@yield('title')</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />


	<!-- CSS Files -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/bootstrap-rtl.css" rel="stylesheet" />
    <link href="/css/material-kit.css" rel="stylesheet"/>

</head>

<body class="profile-page">
	@extends('web.nav')

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('/img/bg-banner.png');">
		</div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
					@yield('content')
	            </div>
	        </div>
		</div>
    </div>
@extends('web.footer')

</body>
	<!--   Core JS Files   -->
	<script src="/js/jquery.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/js/material.min.js"></script>
	<script src="/js/material-kit.js" type="text/javascript"></script>
	<script type="text/javascript">
		$().ready(function(){
			// the body of this function is in assets/material-kit.js
			$(window).on('scroll', materialKit.checkScrollForTransparentNavbar);
		});
	</script>
</html>
