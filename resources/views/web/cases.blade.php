@extends('web.layout')
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
@stop
@section('title')
ک ن ک : تایبەت
@stop
@section('content')
	<h3 class="text-center">.: بابەتی تایبەت :.</h3>
	<hr>
	@foreach($cases as $case)
	<div class="col-md-3">
		<div class=" panel panel-default" >
			<div class="panel-heading" style="height:120px;background-repeat:no-repeat;background-size:100%;background-image:url('uploads/cases/thumb/{{ $case->image }}');"></div>
			<div class="panel-body" style="min-height:80px !important;">
				<a href="{{ URL::to('/cases/'.$case->id) }}" style="font-size:12px;">{{ $case->title }}</a>
			</div>
		</div>
	</div>
	@endforeach
	<div class="clearfix"></div>
	<hr style="margin:5px 0px;border-style:dashed;border-color:#ccc;">
	<div class="col-md-12"><?php echo $cases->render(); ?></div>
@stop