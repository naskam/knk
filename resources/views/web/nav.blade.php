@inject('titlesObject' , 'App\Titles');
<?php $titles = $titlesObject->get_titles(); ?>
<nav class="navbar  navbar-transparent navbar-danger navbar-fixed-top navbar-color-on-scroll">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand pull-left" href="{{ URL::to('/') }}">ک ن ک</a>
        </div>

        <div class="collapse navbar-collapse" id="navigation-example">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="{{ URL::to('/') }}" class="btn">دەستپێک</a>
            </li>
            <li class="dropdown">
          			<a href="#" class="dropdown-toggle" data-toggle="dropdown">کوردستان <b class="caret"></b></a>
          			<ul class="dropdown-menu">
                 @foreach($titles['kurdistan_title'] as $title)
                 <li><a href="{{ URL::to('/kurdistan/'.$title->menu_title ) }}">{{ $title->menu_title }}</a></li>
                 @endforeach
          			</ul>
          	</li>
            <li class="dropdown">
          			<a href="#" class="dropdown-toggle" data-toggle="dropdown">ک ن ک <b class="caret"></b></a>
          			<ul class="dropdown-menu">
                  @foreach($titles['knk_title'] as $title)
                   <li><a href="{{ URL::to('/knk/subject/'.$title->menu_title ) }}">{{ $title->menu_title }}</a></li>
                   @endforeach
                   @foreach($titles['knk_members_title'] as $title)
                   <li><a href="{{ URL::to('/knk/members/'.$title->menu_title ) }}">{{ $title->menu_title }}</a></li>
                   @endforeach
                   <li><a href="{{ URL::to('/knk/members/') }}">ئەندامان</a></li>
          			</ul>
          	</li>
            <li>
              <a href="{{ URL::to('/declarations/') }}" class="btn">بەیاننامە و راگەیەنراو</a>
            </li>
            <li class="dropdown">
          			<a href="#" class="dropdown-toggle" data-toggle="dropdown">تایبەت <b class="caret"></b></a>
          			<ul class="dropdown-menu">
      					       <li><a href="{{ URL::to('/cases') }}">دۆسیە</a></li>
      					       <li><a href="{{ URL::to('/analyze')}}">شیکردنەوە</a></li>
          			</ul>
          	</li>
            <li>
              <a href="{{ URL::to('contact') }}" class="btn">پەیوەندی</a>
            </li>  
          </ul>

           <ul class="nav navbar-nav navbar-left">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}" ><button class="btn btn-info btn-raised btn-sm btn-round">چوونە ژوورەوە</button></a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/dashboard') }}"><i class="fa fa-btn fa-sign-out"></i>داشبورد</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>چوونە دەڕەوە</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
        </div>
    </div>
  </nav>
