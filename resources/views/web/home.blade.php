@extends('web.layout')
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="کۆنگرەی نەتەوەیی کوردستان">
<meta name="keywords" content="کوردستان , نەتەوە , کۆنگرە">
@stop
@section('title')
کۆنگرەی نەتەوەیی کوردستان
@stop
@section('content')
    <div class="row">
    <br>
	<h4 class="text-center ">.: دوایین راگەیەنراو :.</h4><hr>
      <div class="col-md-8">
        @foreach($declarations as $declaration)
          <div class="col-md-12">
                <h5><a href="{{ URL::to('/declarations/'.$declaration->title ) }}">{{$declaration->title}}</a></h5>
                <?php $date = new DateTime($declaration->created_at); ?>
                <p><small>{{ $date->format('Y-m-d') }}</small> .:.
                <small>{{ $declaration->comments->count()}} لێدوان</small></p><div class="clearfix"></div>
                <p><?php $content = strip_tags($declaration->body)?>{{ str_limit($content,$limit='200',$end='...')}}</p>
          <hr style="margin:5px 0px;border-style:dashed;border-color:#ccc;">
          </div> 
        @endforeach
        <br>
        <div class="col-md-12"><?php echo $declarations->render(); ?></div>
    </div>
    <div class="col-md-4"> 
      <div class="panel panel-default">
        <div class="panel-heading">دۆسیەی تایبەت</div>
            <div class="panel-body">
              @foreach($cases as $case)
              <small><a href="{{ URL::to('/cases/'.$case->id) }}">{{ $case->title }}</a></small><hr style="margin:5px 0px;border-style:dotted;border-color:#ccc;">
              @endforeach
            </div>
      </div> 
      <div class="panel panel-default">
        <div class="panel-heading">شرۆڤە</div>
            <div class="panel-body">
              @foreach($analyze as $case)
              <small><a href="{{ URL::to('/analyze/'.$case->id) }}">{{ $case->title }}</a></small><hr style="margin:5px 0px;border-style:dotted;border-color:#ccc;">
              @endforeach
            </div>
      </div>
    </div>
    </div>
@stop
