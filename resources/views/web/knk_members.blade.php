@extends('web.layout')
@section('title')
ک ن ک : ئەندامان
@stop
@section('content')
<div class="row">  
	<h4>{{ $group->type }}</h4>
	<p><?php echo $group->body ?></p>
  <hr style="margin:10px;border-style:dashed;border-color:#ccc">
  @foreach($members as $member)
  <div class="col-md-2">
    <div class="team-player">
	    <img src="/uploads/members/{{ $member->image }}" alt="Thumbnail Image" class="img-rounded img-responsive">
	    <p class="title text-center">{{ $member->name }}
			<br><small>{{ $member->title }}</small>
	    </p>
		@if($member->link)
			<p class="text-center" ><a href="{{ $member->link }}" target="_blank" class="btn btn-info btn-sm btn-raised">لینک بۆ دەڕەوە</a></p>
		@endif
	</div>
   </div>
  @endforeach
</div>
@stop
