@extends('web.layout')
@section('title')
ک ن ک : ڕاگەیێندڕاوەکان
@stop
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="ڕاگەیاندن">
<meta name="keywords" content="کۆنگرەی , نەتەوەیی , کوردستان , ڕاگەیاند">
<style>
.items{
  background:#fff !important;
}
.item{
  padding:5px;
  border-radius:12px;
  margin:3px;
  border:2px dotted #e0e0e0;
  transition:1s;
}
.item:nth-child(odd) {
    background: #f0f0f0;
}
.item:hover{
  background:#ccc !important;
  border-color:#c3c3c3;
}
</style>
@stop
@section('content')
	<div class="col-md-12 items">
  <h3 class="text-center">.: دوایین ڕاگەیاندنەکان :.</h3><hr style="border-style:dashed;border-color:#bbb;">
	@foreach($declarations as $item)
		<div class="col-xs-12 item">
        <a class="item-block" href="{{ URL::to('/declarations/'.$item->title) }}">
          <header>
            <div class="hgroup">
              <h4>{{ $item->title }}</h4>
              <p style="display:inline; margin-right:10px; color:#bbb;font-size:0.8em;">ڕێکەوت : {{ $item->created_at }}</p>
              <p style="display:inline; margin-right:10px; color:#bbb;font-size:0.8em;">لێدوان : </p>
            </div>
          </header>
        </a>
      </div>
	@endforeach
  <div class="clearfix"></div>
  <hr style="margin:12px 0px 5px 0px;border-style:dashed;border-color:transparent;">
  <div class="col-md-12"><?php echo $declarations->render(); ?></div>
	</div>	
@stop