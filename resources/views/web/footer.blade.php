@inject('titlesObject' , 'App\Titles');
<?php $firstKurdistan = $titlesObject->getFirstKurdistan();
      $firstKnk = $titlesObject->getFirstKnk();
 ?>
<footer class="footer">
    <div class="container">
        <nav class="pull-right">
    <ul>
{{--       <li>
        <a href="{{ URL::to('/knk/subject/'.$firstKnk->menu_title) }}">
          {{ $firstKnk->title }}
        </a>
      </li>
      <li>
        <a href="{{ URL::to('/kurdistan/'.$firstKurdistan->menu_title) }}">
           {{ $firstKurdistan->title }}
        </a>
      </li>
 --}}      <li>
        <a href="{{ URL::to('declarations/')}}">
           راگەیەنراو
        </a>
      </li>
      <li>
        <a href="{{ URL::to('/contact') }}">
          پەیوەندی
        </a>
      </li>
    </ul>
        </nav>
        <div class="copyright pull-left">
           Kurdistan National Congress &copy; 1991 - 2016
        </div>
    </div>
</footer>
