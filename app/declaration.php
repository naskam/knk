<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaration extends Model
{
	protected $fillable = ['user_id','title','body','image','tags','updated_at'];
	protected $dates = ['created_at'];
    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }
}
