<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['group_id','title','name','email','image','link','created_at','updated_at'];

    public function member_group()
    {
    	return $this->belongsTo('App\Member_group');
    }
}
