<?php 
namespace App;
use App\kurdistan;
use App\knk;
use App\Member_group;
use App\declaration;

class Titles
{
    function get_titles()
    {
        $kurdistan_title = kurdistan::select('menu_title')->orderBy('id')->get();
        $knk_title = knk::select('menu_title')->get();
        $knk_member_title = member_group::select('menu_title')->get();
        $titles = array('kurdistan_title' =>$kurdistan_title,'knk_title'=>$knk_title,'knk_members_title'=>$knk_member_title);
        return $titles;
    }
    function getFirstKurdistan(){
    	return $kurdistan_first = kurdistan::select('title','menu_title')->first();
    }
    function getFirstKnk(){
        return $knk_first = knk::select('title','menu_title')->first();
    }
}