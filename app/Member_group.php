<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member_group extends Model
{
     protected $table = 'member_group';
     protected $fillable = ['type','menu_title','body','created_at','updated_at'];

     public function member()
     {
     	return $this->hasMany('App\Member' ,'group_id');
     }
}

