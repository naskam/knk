<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Knk extends Model
{
     protected $table = 'knk';
     protected $fillable = ['title','menu_title','body','tags'];
}
