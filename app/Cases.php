<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
   protected $table = "cases";
   protected $fillable = ['user_id','title','body','image','tags','updated_at'];
   protected $dates = ['created_at'];
}
