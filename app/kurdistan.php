<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Kurdistan extends Eloquent
{	
     protected $table = 'kurdistan';
     protected $fillable = ['title','menu_title','body','tags','created_at','updated_at'];
}
