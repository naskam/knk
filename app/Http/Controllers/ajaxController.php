<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Http\Requests;
use App\member_group;
use App\declaration;
use App\kurdistan;
use App\analyze;
use App\member;
use App\contact;
use App\cases;
use App\knk;
use App\user;
use \File;
class ajaxController extends Controller
{
	public function __construct()
    {
         $this->middleware('auth');
    }
   	public function kurdistan_edit($id)
   	{
   		$item = kurdistan::find($id);
   		return view('ajax.kurdistan_edit',compact('item','id'));
   	}
   	public function kurdistan_update($id,kurdistan $kurdistan , Request $request)
   	{
   		if ($kurdistan->find($id)->update($request->all())) {
   			return 'گۆڕانکاری سەرکەوتوو بو.';
   		} else{
   			return 'هەڵە یەک ڕۆی داوە ، غۆڕانکاری روود نەداوە';
   		}

   	}
   	public function kurdistan_delete($id ,kurdistan $kurdistan)
   	{

   		if ($kurdistan->destroy($id)) {
   			return 'گۆڕانکاری سەرکەوتوو بو.';
   		} else{
   			return 'هەڵە یەک ڕۆی داوە ، غۆڕانکاری روود نەداوە';
   		}
   	}
    public function member_group_edit($id)
    {
       $item = member_group::find($id);
       return view('ajax.member_group_edit',compact('item','id'));
    }
   public function member_group_update($id,member_group $member_group , Request $request)
    {
       if ($member_group->find($id)->update($request->all())) {
          return 'گۆڕانکاری سەرکەوتوو بو.';
       } else{
          return 'هەڵە یەک ڕۆی داوە ، غۆڕانکاری روود نەداوە';
       }

    }
   public function member_group_delete($id ,member_group $member_group)
    {

       if ($member_group->destroy($id)) {
          return 'گۆڕانکاری سەرکەوتوو بو.';
       } else{
          return 'هەڵە یەک ڕۆی داوە ، غۆڕانکاری روود نەداوە';
       }
    }
  public function member_edit($id)
  {
    $item = member::find($id);
    $group = member_group::select('type','id')->get();
    return view('ajax.members_edit',compact('item','id','group'));
  }
  public function member_update($id,member $member , Request $request)
  {
     if ($member->find($id)->update($request->all())) {
        return 'گۆڕانکاری سەرکەوتوو بو.';
     } else{
        return 'هەڵە یەک ڕۆی داوە ، غۆڕانکاری روود نەداوە';
     }

  }
  public function member_delete($id,member $member)
  {
    $file = $member->find($id);
    $file ='uploads/members/'.$file->image;
    if ($member->destroy($id)) {
      if(File::delete([$file])){
        return 'ڕەش کردنەوە ئەنجام دڕا.';
      } else {
        return 'ئەنجامەکە سەرکەوتوو نەبو.';
      }
    }
  }
  public function knk_edit($id , knk $knk)
  {
    $item = $knk->find($id);
    return view('ajax.knk_edit',compact('id','item'));
  }
  public function knk_update($id,knk $knk , Request $request)
  {
     if ($knk->find($id)->update($request->all())) {
        return 'گۆڕانکاری سەرکەوتوو بو.';
     } else{
        return 'هەڵە یەک ڕۆی داوە ، گۆڕانکاری سەرکەوتوو نەبو';
     }
  }
  public function knk_delete($id,knk $knk)
  {
    if ($knk->destroy($id)) {
      return 'ڕەش کردنەوە ئەنجام دڕا';
    } else {
      return 'ئەنجامەکە سەرکەوتوو نەبو.';
    }
  }
  public function declaration_edit($id,declaration $declaration)
  {
    $item = $declaration->find($id);
    return view('ajax.declaration_edit',compact('id','item'));
  }
  public function declaration_update($id,declaration $declaration , Request $request)
  {
     if ($declaration->find($id)->update($request->all())) {
        return 'گۆڕانکاری سەرکەوتوو بو.';
     } else{
        return 'هەڵە یەک ڕۆی داوە ، گۆڕانکاری سەرکەوتوو نەبو';
     }
  }
  public function declaration_delete($id,declaration $declaration)
  {
    $file = $declaration->find($id);
    $file ='uploads/declarations/'.$file->image;
    if ($declaration->destroy($id)) {
      if(File::delete([$file])){
        return 'ڕەش کردنەوە ئەنجام دڕا.';
      } else {
        return 'ئەنجامەکە سەرکەوتوو نەبو.';
      }
    }
  }
  public function cases_edit($id,cases $cases)
  {
    $item = $cases->find($id);
    return view('ajax.cases_edit',compact('id','item'));
  }
  public function cases_update($id,cases $cases , Request $request)
  {
     if ($cases->find($id)->update($request->all())) {
        return 'گۆڕانکاری سەرکەوتوو بو.';
     } else{
        return 'هەڵە یەک ڕۆی داوە ، گۆڕانکاری سەرکەوتوو نەبو';
     }
  }
  public function cases_delete($id,cases $cases)
  {
    $file = $cases->find($id);
    $file ='uploads/cases/'.$file->image;
    if ($cases->destroy($id)) {
      if(File::delete([$file])){
        return 'ڕەش کردنەوە ئەنجام دڕا.';
      } else {
        return 'ئەنجامەکە سەرکەوتوو نەبو.';
      }
    }
  }
  public function analyze_edit($id,analyze $analyze)
  {
    $item = $analyze->find($id);
    return view('ajax.analyze_edit',compact('id','item'));
  }
  public function analyze_update($id,analyze $analyze , Request $request)
  {
     if ($analyze->find($id)->update($request->all())) {
        return 'گۆڕانکاری سەرکەوتوو بو.';
     } else{
        return 'هەڵە یەک ڕۆی داوە ، گۆڕانکاری سەرکەوتوو نەبو';
     }
  }
  public function analyze_delete($id,analyze $analyze)
  {
    $file = $analyze->find($id);
    $file ='uploads/analyze/'.$file->image;
    if ($analyze->destroy($id)) {
      if(File::delete([$file])){
        return 'ڕەش کردنەوە ئەنجام دڕا.';
      } else {
        return 'ئەنجامەکە سەرکەوتوو نەبو.';
      }
    }
  }
  public function message_show($id,contact $contact)
  {
    $item = $contact->find($id);
    return view('ajax.message_show',compact('id','item'));
  }
  public function message_delete($id,contact $contact)
  {
    if ($contact->destroy($id)) {
      return 'ڕەش کردنەوە ئەنجام دڕا';
    } else {
      return 'ئەنجامەکە سەرکەوتوو نەبو.';
    }
  }

	public function user_edit($id , user $user)
	{
		$item = $user->find($id);
		return view('ajax.user_edit',compact('id','item'));
	}
	public function user_update($id,user $user , Request $request)
	{
		 if ($user->find($id)->update([
               'name' => $request->input('name'),
               'email' => $request->input('email'),
               'password' => Hash::make($request->input('password'))
               ]))
       {
				return 'گۆڕانکاری سەرکەوتوو بو.';
		 } else{
				return 'هەڵە یەک ڕۆی داوە ، گۆڕانکاری سەرکەوتوو نەبو';
		 }
	}
}
