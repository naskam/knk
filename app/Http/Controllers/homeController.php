<?php
namespace App\Http\Controllers;
// include composer autoload


// import the Intervention Image Manager Class

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Session;
use App\kurdistan;
use App\member_group;
use App\declaration;
use App\analyze;
use App\member;
use App\contact;
use App\cases;
use App\knk;
use App\user;
use Image;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.home');
    }
    public function kurdistan(){
        $kurdistan = kurdistan::select()->paginate(10);
        return view('dashboard.kurdistan',compact('kurdistan'));
    }
    public function kurdistan_insert(Request $request){
        $kurdistan = new kurdistan;
        $kurdistan->create([
            'title' => $request->input('title'),
            'menu_title' => $request->input('menu_title'),
            'body' => $request->input('body'),
            'tags' => $request->input('tags')
            ]
            );
        $kurdistan = kurdistan::select()->paginate(10);
        return view('dashboard.kurdistan',compact('kurdistan'));
    }
public function memberGroup()
{
    $member_group = member_group::select()->paginate(10);
        return view('dashboard.memeberGroup',compact('member_group'));
}
public function knk()
{
    $knk = knk::select()->paginate(10);
    return view('dashboard.knk',compact('knk'));
}
 public function knk_insert(Request $request){
        $knk = new knk;
        $knk->create([
            'title' => $request->input('title'),
            'menu_title' => $request->input('menu_title'),
            'body' => $request->input('body'),
            'tags' => $request->input('tags')
            ]
            );
        $knk = knk::select()->paginate(10);
        return view('dashboard.knk',compact('knk'));
    }
    public function member_group()
    {
        $member_group = member_group::select()->paginate(10);
        return view('dashboard.memberGroup',compact('member_group'));
    }
    public function member_group_insert(Request $request){
        $member_group = new member_group;
        $member_group->create([
            'type' => $request->input('type'),
            'menu_title' => $request->input('menu_title'),
            'body' => $request->input('body')
            ]
            );
        $member_group = member_group::select()->paginate(10);
        return view('dashboard.memberGroup',compact('member_group'));
    }
    public function members()
    {
        $member_group = member_group::all();
        $members = member::with('member_group')->paginate(10);
        return view('dashboard.members',compact('members','member_group'));
    }
     public function members_insert(Request $request)
    {
        $member = new member;
        if (Input::hasFile('image')) {
            $file =Input::file('image');
            $filename = rand().$file->getClientOriginalName();
            $thumb = Image::make(Input::file('image'))->resize(100, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/members/thumb/'.$filename,100);
            $uploaded = Image::make(Input::file('image'))->resize(300, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/members/'.$filename,100);
            if ($uploaded && $thumb) {
                $member->create([
                    'group_id' => $request->input('group_id'),
                    'title'    => $request->input('title'),
                    'name'     => $request->input('name'),
                    'email'    => $request->input('email'),
                    'image'    => $filename,
                    'link'     => $request->input('link'),
                    ]);
            } else { Session::flash('message',"File uploading failed");}
        } else { Session::flash('message',"File Doesn't Exist");}
            return redirect()->to('dashboard/members');
    }
    public function declarations()
    {
        $declarations = declaration::select()->paginate(10);
        return view('dashboard.declarations',compact('declarations'));
    }
    public function declaration_insert(Request $request , declaration $declaration)
    {
        if (Input::hasFile('image')) {
            $file =Input::file('image');
            $filename = rand().$file->getClientOriginalName();
            $thumb = Image::make(Input::file('image'))->resize(100, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/declarations/thumb/'.$filename,100);
            $uploaded = Image::make(Input::file('image'))->resize(null, 300, function ($constraint) { $constraint->aspectRatio();})->save('uploads/declarations/'.$filename,100);
            if ($uploaded && $thumb) {
                $declaration->create([
                    'user_id'  => $request->user()->id,
                    'title'    => $request->input('title'),
                    'body'     => $request->input('body'),
                    'tags'     => $request->input('tags'),
                    'image'    => $filename,
                    ]);
                $request->session()->flash('message', "ئەنجامەکە سەرکەوتو بوو");
            } else { Session::flash('message',"File uploading failed");}
        } else { Session::flash('message',"File Doesn't Exist");}
            return redirect()->to('dashboard/declarations');
    }
    public function cases()
    {
        $cases = cases::select()->paginate(10);
        return view('dashboard.cases',compact('cases'));
    }
    public function cases_insert(Request $request , cases $cases)
    {
        if (Input::hasFile('image')) {
            $file =Input::file('image');
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = rand(1000000,99999999).'.'.$extension;
            $thumb = Image::make(Input::file('image'))->resize(100, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/cases/thumb/'.$filename,100);
            $uploaded = Image::make(Input::file('image'))->resize(1000, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/cases/'.$filename,100);
            if ($uploaded && $thumb) {
                $cases->create([
                    'user_id'  => $request->user()->id,
                    'title'    => $request->input('title'),
                    'body'     => $request->input('body'),
                    'tags'     => $request->input('tags'),
                    'image'    => $filename,
                    ]);
                $request->session()->flash('message', "ئەنجامەکە سەرکەوتو بوو");
            } else { Session::flash('message',"File uploading failed");}
        } else { Session::flash('message',"File Doesn't Exist");}
            return redirect()->to('dashboard/cases');
    }
    public function analyze()
    {
        $analyze = analyze::select()->paginate(10);
        return view('dashboard.analyze',compact('analyze'));
    }
    public function analyze_insert(Request $request , analyze $analyze)
    {
        if (Input::hasFile('image')) {
            $file =Input::file('image');
            $filename = rand().$file->getClientOriginalName();
            $thumb = Image::make(Input::file('image'))->resize(100, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/analyze/thumb/'.$filename,100);
            $uploaded = Image::make(Input::file('image'))->resize(1000, null, function ($constraint) { $constraint->aspectRatio();})->save('uploads/analyze/'.$filename,100);
            if ($uploaded && $thumb) {
                $analyze->create([
                    'user_id'  => $request->user()->id,
                    'title'    => $request->input('title'),
                    'body'     => $request->input('body'),
                    'tags'     => $request->input('tags'),
                    'image'    => $filename,
                    ]);
                Session::flash('message', "ئەنجامەکە سەرکەوتو بوو");
            } else { Session::flash('message',"File uploading failed");}
        } else { Session::flash('message',"File Doesn't Exist");}
            return redirect()->to('dashboard/analyze');
    }
    public function messages(contact $contact)
    {
        $contact = contact::select()->paginate(10);
        return view('dashboard.messages',compact('contact'));
    }
    public function users(user $user)
    {
        $users = user::select()->paginate(10);
        return view('dashboard.users',compact('users'));
    }
    public function user_insert(Request $request){
           $knk = new knk;
           $knk->create([
               'name' => $request->input('name'),
               'email' => $request->input('email'),
               'password' =>  Hash::make($request->input('password'))
               ]);
           Session::flash('message', "ئەنجامەکە سەرکەوتو بوو");
           $users = user::select()->paginate(10);
           return view('dashboard.users',compact('users'));
       }
}
