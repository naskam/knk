<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Validator;
use Redirect;
use App\Member_group;
use App\declaration;
use App\kurdistan;
use Carbon\Carbon;
use App\contact;
use App\member;
use App\analyze;
use App\cases;
use App\knk;

class webController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }
    function get_titles()
    {
        $kurdistan_title = kurdistan::select('menu_title')->orderBy('id')->get();
        $knk_title = knk::select('menu_title')->get();
        $knk_member_title = member_group::select('menu_title')->get();
        $titles = array('kurdistan_title' =>$kurdistan_title,'knk_title'=>$knk_title,'knk_members_title'=>$knk_member_title);
        return $titles;
    }
    public function index()
    {
        $declarations = declaration::select()->orderBy('id','desc')->paginate(5);
        $cases = cases::orderBy('id', 'desc')->select('id','title')->take(5)->get();
        $analyze = analyze::orderBy('id', 'desc')->select('id','title')->take(5)->get();
        return view('web.home',compact('declarations','cases','analyze'));
    }
    public function kurdistan($title)
    {
        $kurdistan = kurdistan::where('menu_title',$title)->first();
        return view('web.kurdistan',compact('kurdistan'));
    }
    public function declarations()
    {
        $declarations = declaration::orderBy('created_at', 'desc')->select('id','title')->paginate(10);
        return view('web.declarations',compact('declarations')); 
    }
    public function show_declaration($title)
    {
        $declaration = declaration::where('title',$title)->first();
        return view('web.declaration',compact('declaration'));   
    }
    public function cases()
    {
        $cases = cases::paginate(8);
        return view('web.cases',compact('cases') );   
    }
    public function case_item($id,cases $cases)
    {
        $case = $cases->find($id);
        return view('web.case_item',compact('case'));
    }
    public function analyze()
    {
        $analyze = analyze::paginate(8);
        return view('web.analyze',compact('analyze'));
    }
    public function analyze_item($id,analyze $analyzes)
    {
        $analyze = $analyzes->find($id);
        return view('web.analyze_item',compact('analyze'));
    }
    public function contact(Request $request)
    {
        return view('web.contact');
    }
    public function contact_insert(Request $request,contact $contact)
    {
        $validator = Validator::make($request->all(), [
        'name'                 => 'required',
        'email'                => 'required|email', 
        'message'              => 'required',
        'g-recaptcha-response' => 'required|captcha'
        ]);
        if ($validator->fails()) {
            Session::flash('msg', "پەمەکەتان سەرکەوتو نەبو ، تکایە دووبارە هەوڵ بدەنەوە");
            return view('web.contact');
        } else {
        $contact->create([
            "name" => $request->input('name'),
            "email" => $request->input('email'),
            "message" => $request->input('message')
            ]);
            Session::flash('message', "پەیامەکەتان بە دەستمان گەیشت ، سپاس بۆ پەیوەندیتان");
         return view('web.contact');
        }
    }
    public function knk_subject($title)
    {
        $knk = knk::where('menu_title',$title)->first();
        return view('web.knk_subject',compact('knk'));
    }
    public function knk_member($title,member $member,member_group $member_group)
    {
        $group =$member_group->where('menu_title',$title)->first();
        $members = $member->where('group_id',$group->id)->paginate(20);
        return view('web.knk_members',compact('group','members'));
    }
    public function members(member $member)
    {
        $members = $member->paginate(24);
        return view('web.members',compact('members'));
    }
}
