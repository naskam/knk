<?php

   Route::group(['middleware' => 'web'] , function(){

    /* Web Routes */
    Route::get('/','webController@index');
    Route::get('/kurdistan/{title}','webController@kurdistan');
    Route::get('knk/subject/{title}','webController@knk_subject');
    Route::get('knk/members/{title}','webController@knk_member');
    Route::get('knk/members/','webController@members');
    Route::get('declarations/{title}','webController@show_declaration');
    Route::get('declarations/','webController@declarations');
    Route::get('cases','webController@cases');
    Route::get('cases/{id}','webController@case_item');
    Route::get('analyze','webController@analyze');
    Route::get('analyze/{id}','webController@analyze_item');
    Route::get('/contact','webController@contact');
    Route::post('/contact','webController@contact_insert');


    /*  Dashboard Routes */
    Route::auth();
    Route::get('/dashboard', 'HomeController@index');
    Route::get('/dashboard/kurdistan', 'HomeController@kurdistan');
    Route::post('/dashboard/kurdistan', 'HomeController@kurdistan_insert');
    Route::get('/dashboard/member_group', 'HomeController@memberGroup');
    Route::get('/dashboard/knk', 'HomeController@knk');
    Route::post('/dashboard/knk', 'HomeController@knk_insert');
    Route::get('/dashboard/member_group', 'HomeController@member_group');
    Route::post('/dashboard/member_group', 'HomeController@member_group_insert');
    Route::get('/dashboard/members', 'HomeController@members');
    Route::post('/dashboard/members', 'HomeController@members_insert');
    Route::get('/dashboard/declarations', 'HomeController@declarations');
    Route::post('/dashboard/declarations', 'HomeController@declaration_insert');
    Route::get('/dashboard/cases', 'HomeController@cases');
    Route::post('/dashboard/cases', 'HomeController@cases_insert');
    Route::get('/dashboard/analyze', 'HomeController@analyze');
    Route::post('/dashboard/analyze', 'HomeController@analyze_insert');
    Route::get('/dashboard/messages', 'HomeController@messages');
    Route::get('/dashboard/users', 'HomeController@users');
    Route::post('/dashboard/users', 'HomeController@user_insert');


    //* Ajax Routes *//
    Route::get('/ajax/kurdistan/edit/{id}','ajaxController@kurdistan_edit');
    Route::patch('/ajax/kurdistan/update/{id}','ajaxController@kurdistan_update');
    Route::delete('/ajax/kurdistan/delete/{id}','ajaxController@kurdistan_delete');
    Route::get('/ajax/member_group/edit/{id}','ajaxController@member_group_edit');
    Route::patch('/ajax/member_group/update/{id}','ajaxController@member_group_update');
    Route::delete('/ajax/member_group/delete/{id}','ajaxController@member_group_delete');
    Route::get('/ajax/member/edit/{id}','ajaxController@member_edit');
    Route::patch('/ajax/member/update/{id}','ajaxController@member_update');
    Route::delete('/ajax/member/delete/{id}','ajaxController@member_delete');
    Route::get('/ajax/knk/edit/{id}','ajaxController@knk_edit');
    Route::patch('/ajax/knk/update/{id}','ajaxController@knk_update');
    Route::delete('/ajax/knk/delete/{id}','ajaxController@knk_delete');
    Route::get('/ajax/declaration/edit/{id}','ajaxController@declaration_edit');
    Route::patch('/ajax/declaration/update/{id}','ajaxController@declaration_update');
    Route::delete('/ajax/declaration/delete/{id}','ajaxController@declaration_delete');
    Route::get('/ajax/cases/edit/{id}','ajaxController@cases_edit');
    Route::patch('/ajax/cases/update/{id}','ajaxController@cases_update');
    Route::delete('/ajax/cases/delete/{id}','ajaxController@cases_delete');
    Route::get('/ajax/analyze/edit/{id}','ajaxController@analyze_edit');
    Route::patch('/ajax/analyze/update/{id}','ajaxController@analyze_update');
    Route::delete('/ajax/analyze/delete/{id}','ajaxController@analyze_delete');
    Route::get('/ajax/message/show/{id}','ajaxController@message_show');
    Route::delete('/ajax/message/delete/{id}','ajaxController@message_delete');
    Route::get('/ajax/user/edit/{id}','ajaxController@user_edit');
    Route::patch('/ajax/user/update/{id}','ajaxController@user_update');
    Route::delete('/ajax/user/delete/{id}','ajaxController@user_delete');

    });
