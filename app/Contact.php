<?php

namespace App;
use Validator;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
   protected $table = "contact";
   protected $fillable = ['name','email','message'];
   protected $dates = ['created_at'];

   public static $rules = array(
   		'name'                 => 'required',
        'email'                => 'required|email', 
        'message'              => 'required',
        'g-recaptcha-response' => 'required|captcha'
   	);
   public static function validate($data){
   		return Validator::make($data, static::$rules);
   }
}
