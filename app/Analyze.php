<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analyze extends Model
{
   protected $table = "analyze";
   protected $fillable = ['user_id','title','body','image','tags','updated_at'];
   protected $dates = ['created_at'];
}