<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        // Insert some stuff
       DB::table('users')->insert(
           array(
             'name' => 'Unknown',
             'email'=> '	r.gefur@gmail.com',
             'password' => '$2y$10$od5RMTuWv.1jZ98jTY/rbecWw0mlIP5zRzF1Wf7gs.Yb24lAfPaBq'
           )
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
