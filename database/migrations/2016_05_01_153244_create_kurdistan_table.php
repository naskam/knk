<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurdistanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurdistan',function(Blueprint $table){
          $table->increments('id');
          $table->string('title');
          $table->string('menu_title')->unique();
          $table->longtext('body');
          $table->string('tags');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kurdistan');
    }
}
