<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special',function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->string('title');
          $table->string('menu_title');
          $table->string('tags');
          $table->longtext('body');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special');
    }
}
